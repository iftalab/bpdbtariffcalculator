package com.iftalab.qtapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Spinner spinner;
    private EditText etUnit;
    private TextView tvResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);
        spinner = findViewById(R.id.spinner);
        etUnit = findViewById(R.id.etUnit);
        tvResult = findViewById(R.id.tvResult);
        etUnit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    double unitConsumed = Double.parseDouble(s.toString());
                    String result = calculateBill(1, unitConsumed);
                    tvResult.setText(result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private String calculateBill(int loadInKiloWatt, double unitConsumed) {
        double energyCharge = 0;
        double unitLeftToCalculate = unitConsumed;
        if (unitConsumed <= 50) {
            energyCharge = unitConsumed * 3.5;
        } else {
            if (unitConsumed >= 75) {
                energyCharge += 75 * 4.0;
                unitLeftToCalculate -= 75;
            } else {
                energyCharge += unitLeftToCalculate * 4.0;
            }
            if (unitConsumed >= 200) {
                energyCharge += 125 * 5.45;
                unitLeftToCalculate -= 125;
            }else {
                energyCharge = unitLeftToCalculate * 5.45;
            }
            if (unitConsumed >= 300) {
                energyCharge += 100 * 5.70;
                unitLeftToCalculate -= 125;
            }else {
                energyCharge = unitLeftToCalculate * 5.70;
            }
            if (unitConsumed >= 200) {
                energyCharge += 125 * 5.45;
                unitLeftToCalculate -= 125;
            }else {
                energyCharge = unitLeftToCalculate * 5.45;
            }
        }

        
        double demandCharge = 25 * loadInKiloWatt;
        double netBill = energyCharge + demandCharge;
        double totalBill = netBill + netBill * 0.05;
        return "Your bill is " + totalBill + " taka";
    }
}
